#ifndef MOVIE_MARATHON_OPERATIONHANDLER_H
#define MOVIE_MARATHON_OPERATIONHANDLER_H

#include <stdbool.h>
#include <stdio.h>
#include "constants.h"
#include "user.h"

#define ERROR_MESSAGE "ERROR\n"
#define OK_MESSAGE "OK\n"
#define NONE_MESSAGE "NONE\n"

void printOK();

void printError();

void printNone();

void handleAddUser(User users[USER_ARRAY_SIZE], unsigned short parentUserId, unsigned short userId);

void handleDeleteUser(User users[USER_ARRAY_SIZE], unsigned short userId);

void handleAddMovie(User users[USER_ARRAY_SIZE], unsigned short userId, long movieRating);

void handleDeleteMovie(User users[USER_ARRAY_SIZE], unsigned short userId, long movieRating);

void handleMarathon(User users[USER_ARRAY_SIZE], unsigned short userId, long k);

#endif //MOVIE_MARATHON_OPERATIONHANDLER_H
