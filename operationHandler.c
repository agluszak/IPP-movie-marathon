#include <stddef.h>
#include "operationHandler.h"
#include "inputHandler.h"
#include "movieList.h"

// This module contains methods which operate on formally correct data.
// These methods take care of displaying output to the user and updating the users tree.

void printOK() {
    fprintf(stdout, OK_MESSAGE);
}

void printError() {
    fprintf(stderr, ERROR_MESSAGE);
}

void printNone() {
    fprintf(stdout, NONE_MESSAGE);
}

void handleAddUser(User users[USER_ARRAY_SIZE], unsigned short parentUserId, unsigned short userId) {
    if (users[parentUserId] && !users[userId]) {
        users[userId] = createUser(users[parentUserId]);
        printOK();
    } else {
        printError();
    }
}

void handleDeleteUser(User users[USER_ARRAY_SIZE], unsigned short userId) {
    if (userId != 0 && users[userId]) {
        deleteUser(users[userId]);
        users[userId] = NULL;
        printOK();
    } else {
        printError();
    }
}

void handleAddMovie(User users[USER_ARRAY_SIZE], unsigned short userId, long movieRating) {
    if (users[userId]) {
        if (addMovie(users[userId], movieRating)) {
            printOK();
        } else {
            printError();
        }
    } else {
        printError();
    }
}

void handleDeleteMovie(User users[USER_ARRAY_SIZE], unsigned short userId, long movieRating) {
    if (users[userId]) {
        if (deleteMovie(users[userId], movieRating)) {
            printOK();
        } else {
            printError();
        }
    } else {
        printError();
    }
}

void handleMarathon(User users[USER_ARRAY_SIZE], unsigned short userId, long k) {
    if (users[userId]) {
        MovieListNode result = marathon(users[userId], k);
        if (result != NULL) {
            printMovieList(result);
        } else {
            printNone();
        }
    } else {
        printError();
    }
}
