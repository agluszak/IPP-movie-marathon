CC=gcc
CC_PARAMS=-std=c11 -Wall -Wextra -O2
CC_PARAMS_DEBUG=-std=c11 -Wall -Wextra -g
VALGRIND=valgrind
VALGRIND_PARAMS=--error-exitcode=15 --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all

all: main

main: main.c user.o movieList.o inputHandler.o operationHandler.o
	$(CC) $(CC_PARAMS) main.c user.o movieList.o inputHandler.o operationHandler.o -o main
user.o: user.c user.h movieList.h constants.h  inputHandler.h
	$(CC) $(CC_PARAMS) -c user.c
movieList.o: movieList.c movieList.h
	$(CC) $(CC_PARAMS) -c movieList.c
inputHandler.o: inputHandler.c inputHandler.h constants.h
	$(CC) $(CC_PARAMS) -c inputHandler.c
operationHandler.o: operationHandler.c operationHandler.h user.h constants.h
	$(CC) $(CC_PARAMS) -c operationHandler.c

debug: main.c user.debug.o movieList.debug.o inputHandler.debug.o operationHandler.debug.o
	$(CC) $(CC_PARAMS_DEBUG) main.c user.debug.o movieList.debug.o inputHandler.debug.o operationHandler.debug.o -o debug
user.debug.o: user.c user.h movieList.h constants.h  inputHandler.h
	$(CC) $(CC_PARAMS_DEBUG) -c user.c -o user.debug.o
movieList.debug.o: movieList.c movieList.h
	$(CC) $(CC_PARAMS_DEBUG) -c movieList.c -o movieList.debug.o
inputHandler.debug.o: inputHandler.c inputHandler.h constants.h
	$(CC) $(CC_PARAMS_DEBUG) -c inputHandler.c -o inputHandler.debug.o
operationHandler.debug.o: operationHandler.c operationHandler.h user.h constants.h
	$(CC) $(CC_PARAMS_DEBUG) -c operationHandler.c -o operationHandler.debug.o

valgrind: debug
	$(VALGRIND) $(VALGRIND_PARAMS) ./debug

clean:
	-rm -f *.o main debug
	
.PHONY: clean valgrind