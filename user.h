#ifndef MOVIE_MARATHON_USER_H
#define MOVIE_MARATHON_USER_H

#include <stdbool.h>
#include "movieList.h"

// Users are implemented as nodes in a cyclic list.
// 'next' points to the right brother of a given user, or -- if he is the last child of his parent --
// to his parent.
// 'previous' works analogously.
// If user has no children, then his firstChild and lastChild will be himself.
// If you follow 'next' links of user's firstChild, you should finally reach the user you started with.
// Movies proposed by user are stored in a sorted, singly-linked list.
struct UserStruct {
    struct MovieListNodeStruct* movieHead;
    struct UserStruct* previous;
    struct UserStruct* next;
    struct UserStruct* firstChild;
    struct UserStruct* lastChild;
};

typedef struct UserStruct* User;

User createRootUser();

User createUser(User parent);

bool addMovie(User user, long movieRating);

bool deleteMovie(User user, long movieRating);

MovieListNode marathon(User user, long k);

void deleteUser(User user);

void destroyTree(User user);

#endif //MOVIE_MARATHON_USER_H
