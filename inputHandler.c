#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <values.h>
#include <errno.h>
#include "inputHandler.h"
#include "constants.h"

// Returns true if 'string' starts with 'substring'.
static bool startsWithString(char* string, char* substring) {
    return !strncmp(string, substring, strlen(substring));
}

// Returns true if 'string' (assumed to represent a number) has no leading zeros.
static bool numberHasNoLeadingZeros(char* string) {
    bool foundZero = false;
    bool foundNonZero = false;
    for (size_t i = 0; i < strcspn(string, INPUT_SEPARATOR_STRING); i++) {
        if (*(string + i) == ZERO_CHAR) {
            if (foundZero) {
                return foundNonZero;
            } else {
                foundZero = true;
            }
        } else {
            foundNonZero = true;
        }
    }
    return true;
}

// Returns true if the first char of 'string' is a digit.
static bool startsWithDigit(char* string) {
    return strcspn(string, DIGITS) == 0;
}

// Extracts a number from 'input', sets endPos to the next char after the number.
// There must be no leading zeros and leading whitespaces.
// If the extraction was successful it returns true
// and sets the value pointed by 'paramPtr' to the extracted number.
static bool getNumber(char* input, char** endPosPtr, long paramMax, long* paramPtr) {
    long firstParameter = strtol(input, endPosPtr, 10);
    if (*endPosPtr == input) {
        // strtol couldn't find a number
        return false;
    } else if (!startsWithDigit(input)) {
        // strtol found a number, but it was preceded by spaces or it was negative
        return false;
    } else if (!numberHasNoLeadingZeros(input)) {
        // number had leading zeros
        return false;
    } else if ((firstParameter == LONG_MAX || firstParameter == LONG_MIN) && errno == ERANGE) {
        // number was too long to be even parsed
        return false;
    } else if (firstParameter < MIN_ID || firstParameter > paramMax) {
        // number was greater than provided maximum
        return false;
    } else {
        // the number was correct
        *paramPtr = firstParameter;
        return true;
    }
}

// This functions tries to parse an input with one parameter
// Correct input should look like this:
// "<command> <number>\n"
// where number must be parsed by function getNumber and smaller than paramMax
// If the input is correct, it returns true and sets the variable pointed by paramPtr to the extracted number
// If anything goes wrong, it returns false
bool handleInputWithOneParam(char* input,
                             char* command,
                             long paramMax,
                             long* paramPtr) {
    if (startsWithString(input, command)) {
        char* pos = input + strlen(command);
        char* endPos = pos;

        if (startsWithString(pos, INPUT_SEPARATOR_STRING)) {
            char* numberStartPos = pos + INPUT_SEPARATOR_STRING_LENGTH;
            if (getNumber(numberStartPos, &endPos, paramMax, paramPtr)) {
                if (*endPos != NEWLINE_CHAR) {
                    // input doesn't end with a newline
                    return false;
                } else {
                    // input is correct
                    return true;
                }
            }
        } else {
            // there is no space before parameter
            return false;
        }
    }
    return false;
}

// This functions tries to parse an input with two parameters
// Correct input should look like this:
// "<command> <number1> <number2>\n"
// where numbers must be parsed by function getNumber
// and smaller than firstParamMax and secondParamMax respectively
// If the input is correct, it returns true and sets the variable pointed by firstParamPtr to first extracted value
// and variable pointed by secondParamPtr to second extracted value
// If anything goes wrong, it returns false
bool handleInputWithTwoParams(char* input,
                              char* command,
                              long firstParamMax,
                              long secondParamMax,
                              long* firstParamPtr,
                              long* secondParamPtr) {
    if (startsWithString(input, command)) {
        char* pos = input + strlen(command);
        char* firstNumberEndPos = pos;

        if (startsWithString(pos, INPUT_SEPARATOR_STRING)) {
            char* firstNumberStartPos = pos + INPUT_SEPARATOR_STRING_LENGTH;
            if (getNumber(firstNumberStartPos, &firstNumberEndPos, firstParamMax, firstParamPtr)) {
                if (startsWithString(firstNumberEndPos, INPUT_SEPARATOR_STRING)) {
                    char* secondNumberStartPos = firstNumberEndPos + INPUT_SEPARATOR_STRING_LENGTH;
                    char* secondNumberEndPost = secondNumberStartPos;
                    if (getNumber(secondNumberStartPos, &secondNumberEndPost, secondParamMax, secondParamPtr)) {
                        if (*secondNumberEndPost != NEWLINE_CHAR) {
                            // input doesn't end with newline
                            return false;
                        } else {
                            // input is correct
                            return true;
                        }
                    }
                } else {
                    // parameters are not separated with a space
                    return false;
                }
            }
        } else {
            // there is no space before first parameter
            return false;
        }
    }
    return false;
}
