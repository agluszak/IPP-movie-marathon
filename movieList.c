#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "movieList.h"

// Creates and returns a new movie with rating 'rating'.
MovieListNode createMovieNode(long rating) {
    MovieListNode node = (MovieListNode) malloc(sizeof(struct MovieListNodeStruct));
    if (!node) exit(1);
    node->rating = rating;
    node->next = NULL;
    node->mergeNext = NULL;
    return node;
}

// Inserts a new movie with rating 'rating' into the list pointed by 'nodePtr'.
// Returns false if movie with such rating was already present in the list.
// Returns true on success.
bool insertMovieNode(MovieListNode* nodePtr, long rating) {
    MovieListNode temp = *nodePtr;

    if (!temp || temp->rating < rating) {
        MovieListNode newNode = createMovieNode(rating);
        newNode->next = temp;
        *nodePtr = newNode;
        return true;
    } else {
        while (temp->next != NULL && temp->next->rating >= rating) {
            temp = temp->next;
        }
        if (temp->rating == rating) {
            return false;
        } else {
            MovieListNode newNode = createMovieNode(rating);
            newNode->next = temp->next;
            temp->next = newNode;
            return true;
        }
    }
}


// Deletes movie with rating 'rating' from the list pointed by 'nodePtr'.
// Returns false if there was no such movie.
// Returns true on success.
bool deleteMovieNode(MovieListNode* nodePtr, long rating) {
    MovieListNode temp = *nodePtr;
    MovieListNode previous = *nodePtr;

    if (temp && temp->rating == rating) {
        *nodePtr = temp->next;
        free(temp);
        return true;
    }

    while (temp && temp->rating > rating) {
        previous = temp;
        temp = temp->next;
    }

    if (!temp || temp->rating != rating) {
        return false;
    } else {
        previous->next = temp->next;
        free(temp);
        return true;
    }
}

// Returns at most k best movies from target and source, linked by 'mergeNext', sorted in descending order.
// Target must be a result of a previous marathon (i.e. a list linked by 'mergeNext').
// Source must be a list of movies of a particular user (linked by 'next').
// Movies taken from 'source' will have higher rating than 'minRating'.
// This function is recursive.
MovieListNode mergeK(MovieListNode targetHead, MovieListNode sourceHead, long k, long minRating) {
    if (k <= 0) {
        return NULL;
    } else {
        if (targetHead == NULL) {
            if (sourceHead == NULL || sourceHead->rating <= minRating) {
                return NULL;
            } else {
                sourceHead->mergeNext = mergeK(NULL, sourceHead->next, k - 1, minRating);
                return sourceHead;
            }
        } else {
            if (sourceHead == NULL || sourceHead->rating <= minRating) {
                targetHead->mergeNext = mergeK(targetHead->mergeNext, NULL, k - 1, minRating);
                return targetHead;
            } else {
                if (targetHead->rating > sourceHead->rating) {
                    targetHead->mergeNext = mergeK(targetHead->mergeNext, sourceHead, k - 1, minRating);
                    return targetHead;
                } else if (targetHead->rating < sourceHead->rating) {
                    sourceHead->mergeNext = mergeK(targetHead, sourceHead->next, k - 1, minRating);
                    return sourceHead;
                } else {
                    targetHead->mergeNext = mergeK(targetHead->mergeNext, sourceHead->next, k - 1, minRating);
                    return targetHead;
                }
            }
        }
    }
}

// Prints a list (linked by 'mergeNext') starting in 'node'.
void printMovieList(MovieListNode node) {
    MovieListNode temp = node;

    while (temp != NULL) {
        if (temp->mergeNext != NULL) {
            printf("%ld ", temp->rating);
        } else {
            printf("%ld\n", temp->rating);
        }
        temp = temp->mergeNext;
    }
}

// Destroys a list (linked by 'next') starting in 'node' and frees all memory.
void destroyMovieList(MovieListNode node) {
    MovieListNode temp = node;

    while (temp != NULL) {
        MovieListNode next = temp->next;
        free(temp);
        temp = next;
    }
}
