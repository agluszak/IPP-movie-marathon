#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "user.h"
#include "constants.h"

// Returns the root of the users tree.
User createRootUser() {
    User user = (User) malloc(sizeof(struct UserStruct));
    if (!user) exit(1);
    user->movieHead = NULL;
    user->next = user;
    user->previous = user;
    user->firstChild = user;
    user->lastChild = user;
    return user;
}

// Creates a new user and places him in his parent's children list.
User createUser(User parent) {
    assert(parent);
    User user = (User) malloc(sizeof(struct UserStruct));
    if (!user) exit(1);
    user->movieHead = NULL;
    //  parent has no children
    if (parent->firstChild == parent && parent->lastChild == parent) {
        parent->firstChild = user;
        parent->lastChild = user;
        user->previous = parent;
        user->next = parent;
    } else {
    //  parent has at least one child
        parent->lastChild->next = user;
        user->previous = parent->lastChild;
        parent->lastChild = user;
        user->next = parent;

    }
    user->firstChild = user;
    user->lastChild = user;
    return user;
}

// Inserts a movie with rating 'movieRating' into user's movie list.
// Returns false if movie was already present.
// Returns true on success.
bool addMovie(User user, long movieRating) {
    return insertMovieNode(&user->movieHead, movieRating);
}

// Deletes a movie with rating 'movieRating' from user's movie list,
// Returns false if there was no movie with such rating,
// Returns true on success,
bool deleteMovie(User user, long movieRating) {
    return deleteMovieNode(&user->movieHead, movieRating);
}

// A helper function for marathon.
// Combines the previousMarathon result with user's movie list and runs recursively for his children.
// Returns a pointer to the head of result for 'user'.
// The result is a list linked by 'mergeNext'.
static MovieListNode marathonAux(User user, MovieListNode previousMarathon, long k, long previousMax) {
    MovieListNode myMovies = user->movieHead;
    long myBestMovieRating = (myMovies != NULL ? myMovies->rating : MOVIE_GUARDIAN_ID);
    long max = (myBestMovieRating > previousMax ? myBestMovieRating : previousMax);
    MovieListNode marathonResult = mergeK(previousMarathon, myMovies, k, previousMax);

    User childrenIterator = user->firstChild;
    while (childrenIterator != user) {
        marathonResult = marathonAux(childrenIterator, marathonResult, k, max);
        childrenIterator = childrenIterator->next;
    }

    return marathonResult;
}

// Returns the result of marathon for 'user', as specified in the exercise description.
MovieListNode marathon(User user, long k) {
    MovieListNode result = marathonAux(user, NULL, k, MOVIE_GUARDIAN_ID);
    return result;
}

// Deletes user.
// His movies are forgotten, while his children become children of his parent.
void deleteUser(User user) {
    if (user) {
        User *previousUserNextPtr = (user->previous->firstChild == user) ?
                          &user->previous->firstChild :
                          &user->previous->next;
        User *nextUserPreviousPtr = (user->next->lastChild == user) ?
                          &user->next->lastChild :
                          &user->next->previous;
        // user has no children
        if (user->firstChild == user && user->lastChild == user) {
            *previousUserNextPtr = user->next;
            *nextUserPreviousPtr = user->previous;
        } else {
        // user has children, so we put them in his place
            *previousUserNextPtr = user->firstChild;
            user->firstChild->previous = user->previous;
            *nextUserPreviousPtr = user->lastChild;
            user->lastChild->next = user->next;
        }
        destroyMovieList(user->movieHead);
        free(user);
    }
}

// Traverses the tree down from the given user and frees memory.
void destroyTree(User user) {
    if (user) {
        destroyMovieList(user->movieHead);
        User childToDelete = user->firstChild;
        while (childToDelete != user) {
            User next = childToDelete->next;
            destroyTree(childToDelete);
            childToDelete = next;
        }
        free(user);
    }
}
