#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "user.h"
#include "inputHandler.h"
#include "operationHandler.h"

void cleanAtExit(void* data);

int main() {
    // we must create an array allowing constant time access to nodes at a particular depth
    User users[USER_ARRAY_SIZE];
    for (int i = 0; i < USER_ARRAY_SIZE; i++) {
        users[i] = NULL;
    }

    // user0 is the root of our whole tree
    User user0 = createRootUser();
    // when we exit, we want to free all memory used by the program,
    // so we need to delete the whole tree, starting at the root
    cleanAtExit(user0);
    users[0] = user0;

    // we don't need to care about lines longer than MAX_INPUT_LENGTH, because they cannot be correct
    char input[MAX_INPUT_LENGTH];
    memset(input, 0, MAX_INPUT_LENGTH);

    // in these variables we will store parameters extracted from input
    long firstParam;
    long secondParam;

    while (fgets(input, MAX_INPUT_LENGTH - 1, stdin)) {
        // we read char by char from stdin until we find a newline
        int rest = input[strlen(input) - 1];
        while (rest != NEWLINE_CHAR && !feof(stdin)) {
            rest = fgetc(stdin);
        }

        if (input[0] == IGNORE_LINE_MARKER_CHAR) {
            // ignore
        } else if (input[0] == NEWLINE_CHAR) {
            // ignore
        } else if (handleInputWithTwoParams(input, ADD_USER, MAX_USER_ID, MAX_USER_ID,
                                            &firstParam, &secondParam)) {
            handleAddUser(users, (unsigned short) firstParam, (unsigned short) secondParam);
        } else if (handleInputWithOneParam(input, DELETE_USER, MAX_USER_ID, &firstParam)) {
            handleDeleteUser(users, (unsigned short) firstParam);
        } else if (handleInputWithTwoParams(input, ADD_MOVIE, MAX_USER_ID, MAX_MOVIE_ID,
                                            &firstParam, &secondParam)) {
            handleAddMovie(users, (unsigned short) firstParam, (long) secondParam);
        } else if (handleInputWithTwoParams(input, DELETE_MOVIE, MAX_USER_ID, MAX_MOVIE_ID,
                                            &firstParam, &secondParam)) {
            handleDeleteMovie(users, (unsigned short) firstParam, (long) secondParam);
        } else if (handleInputWithTwoParams(input, MARATHON, MAX_USER_ID, MAX_MOVIE_ID,
                                            &firstParam, &secondParam)) {
            handleMarathon(users, (unsigned short) firstParam, (long) secondParam);
        } else {
            // command was incorrect
            printError();
        }
    }
    return 0;
}

static void clean() {
    cleanAtExit(NULL);
}

// Because function atexit doesn't take parameters
// and we need to pass user0 created in the main function to the destroyTree function,
// we use a static variable in which we save pointer to the root during the initial call.
// Then we register function clean to be called at exit.
void cleanAtExit(void* data) {
    static void* root;

    if (data != NULL) {
        root = data;
        atexit(clean);
    } else {
        destroyTree(root);
    }
}
