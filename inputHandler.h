#ifndef MOVIE_MARATHON_INPUTHANDLER_H
#define MOVIE_MARATHON_INPUTHANDLER_H

#include <stdbool.h>

#define MAX_INPUT_LENGTH 40
#define ADD_USER "addUser"
#define DELETE_USER "delUser"
#define ADD_MOVIE "addMovie"
#define DELETE_MOVIE "delMovie"
#define MARATHON "marathon"

#define INPUT_SEPARATOR_STRING " "
#define INPUT_SEPARATOR_STRING_LENGTH (sizeof(INPUT_SEPARATOR_STRING) - 1)

#define NEWLINE_CHAR '\n'
#define IGNORE_LINE_MARKER_CHAR '#'
#define ZERO_CHAR '0'

#define DIGITS "0123456789"

bool handleInputWithOneParam(char* input,
                             char* command,
                             long paramMax,
                             long* paramPtr);

bool handleInputWithTwoParams(char* input,
                              char* command,
                              long firstParamMax,
                              long secondParamMax,
                              long* firstParamPtr,
                              long* secondParamPtr);

#endif //MOVIE_MARATHON_INPUTHANDLER_H
