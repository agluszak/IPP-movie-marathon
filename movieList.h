#ifndef MOVIE_MARATHON_MOVIELIST_H
#define MOVIE_MARATHON_MOVIELIST_H

#include <stdbool.h>

// This structure represents a movie.
// 'next' is a pointer to the next movie in a user's list.
// 'mergeNext' is used during marathon
struct MovieListNodeStruct {
    long rating;
    struct MovieListNodeStruct* next;
    struct MovieListNodeStruct* mergeNext;
};

typedef struct MovieListNodeStruct* MovieListNode;

MovieListNode createMovieNode(long rating);

bool insertMovieNode(MovieListNode* nodePtr, long rating);

bool deleteMovieNode(MovieListNode* nodePtr, long rating);

MovieListNode mergeK(MovieListNode targetHead, MovieListNode sourceHead, long k, long minRating);

void printMovieList(MovieListNode node);

void destroyMovieList(MovieListNode node);

#endif //MOVIE_MARATHON_MOVIELIST_H
